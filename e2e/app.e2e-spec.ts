import { PlPage } from './app.po';

describe('pl App', () => {
  let page: PlPage;

  beforeEach(() => {
    page = new PlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
